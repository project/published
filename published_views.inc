<?php


/***
*       Add Views support
* 
* 
*/

/**
 * Implementation of hook_views_tables().
 *
 */
function published_views_tables() {
  $tables = array();
  $tables['published'] = array(
    'name' => 'published',
    'provider' => 'published',
    'join' => array(
      'left' => array(
        'table' => 'node',
        'field' => 'nid',
       ),
       'right' => array(
         'field' => 'nid',
       ),
    ),
    "sorts" => array(
      'first_delivered' => array(
        'name' => t('Node: Published Time'),
        'field' => array('published'),
        'help' => t('Order nodes by when they were published.'),
      ),
    ),
    "fields" => array(
      'published' => array(
        'name' => t('Node: Published Time'),
        'handler' => views_handler_field_dates(),
        'sortable' => TRUE,
        'help' => t('Display when node was published.'),
      ),
    ),
  );

  return $tables;
} 
 
/**
 * Set string value to be displayed in view. If NID exists, node is queued.
 */
function _published_views_handler_queue($fieldinfo, $fielddata, $value, $data) {

}
  
?>